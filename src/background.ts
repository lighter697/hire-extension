import {combineLatest, Observable, of} from "rxjs";
import {distinctUntilChanged, filter, map, mergeMap, tap} from "rxjs/operators";
import * as moment from "moment";
import Cookie = chrome.cookies.Cookie;

const hostUrl = 'https://api.ollsent-crm.com';

getTabChanged().pipe(
    filter(tab => tab.url),
    distinctUntilChanged((prev, curr) => prev.url === curr.url),
    tap((tab) => {
        chrome.tabs.sendMessage(tab.tabId, {status: 'start'});
    }),
    map((tab) => {
        const matches = decodeURI(tab.url).match(/^(https:\/\/www.linkedin.com\/in\/)([a-zA-Zа-яА-Яёіїґ0-9 \-]+)\/$/i);
        return {...tab, matches: matches};
    }),
    filter((result) => !!result.matches),
    filter((result) => result.matches.length > 2),
    map((result) => {
        return {...result, username: result.matches[2]};
    }),
    mergeMap(tab => combineLatest(of(tab), getCookies())),
    mergeMap(([tab, cookie]) => combineLatest(
        get('https://www.linkedin.com/voyager/api/identity/profiles/' + tab.username + '/profileView', {'csrf-token': cookie}),
        get('https://www.linkedin.com/voyager/api/identity/profiles/' + tab.username + '/positionGroups', {'csrf-token': cookie}),
        get('https://www.linkedin.com/voyager/api/identity/profiles/' + tab.username + '/featuredSkills?includeHiddenEndorsers=true&count=50', {'csrf-token': cookie}),
        get('https://www.linkedin.com/voyager/api/identity/profiles/' + tab.username + '/profileContactInfo', {'csrf-token': cookie}),
        get('https://www.linkedin.com/voyager/api/identity/profiles/' + tab.username + '/recommendations?q=received', {'csrf-token': cookie}),
        of(tab.username),
        of(tab),
    )),
    // filter(([profileView, positionView, profileSkills, profileContacts, profileRecommendations, username]) => profileContacts.emailAddress),
    // Search profile in the database
    mergeMap(([profileView, positionView, profileSkills, profileContacts, profileRecommendations, username, tab]) => {
        return combineLatest(
            of(profileView),
            of(positionView),
            of(profileSkills),
            of(profileContacts),
            of(profileRecommendations),
            of(username),
            post(hostUrl + '/profile/find', JSON.stringify({
                firstName: profileView.profile.firstName,
                lastName: profileView.profile.lastName,
                email: profileContacts.emailAddress,
            }), {'Content-type': 'application/json; charset=utf-8'}),
            of(tab),
        );
    }),
    // Fetch image picture
    mergeMap(([profileView, positionView, profileSkills, profileContacts, profileRecommendations, username, profile, tab]) => {
        let rootUrl;
        let image;
        if (profileView.profile.miniProfile.hasOwnProperty('picture')) {
            if (profileView.profile.miniProfile.picture.hasOwnProperty('com.linkedin.common.VectorImage')) {
                if (profileView.profile.miniProfile.picture['com.linkedin.common.VectorImage'].hasOwnProperty('rootUrl')) {
                    rootUrl = profileView.profile.miniProfile.picture['com.linkedin.common.VectorImage']['rootUrl'];
                }
                if (profileView.profile.miniProfile.picture['com.linkedin.common.VectorImage'].hasOwnProperty('artifacts')) {
                    image = profileView.profile.miniProfile.picture['com.linkedin.common.VectorImage']['artifacts'][3]['fileIdentifyingUrlPathSegment'];
                }
            }
        }
        return combineLatest(
            of(profileView),
            of(positionView),
            of(profileSkills),
            of(profileContacts),
            of(profileRecommendations),
            (profile.hasOwnProperty('avatar') ? of(null) : (rootUrl && image ? getImage(rootUrl + image) : of(null))),
            of(username),
            of(profile),
            of(tab),
        )
    }),
    mergeMap(([profileView, positionView, profileSkills, profileContacts, profileRecommendations, binary, username, profile, tab]) => {
        const formData = new FormData();
        formData.append('imageFile[file]', binary);
        return combineLatest(
            of(profileView),
            of(positionView),
            of(profileSkills),
            of(profileContacts),
            of(profileRecommendations),
            (binary ? post(hostUrl + '/image', formData) : of(null)),
            of(username),
            of(profile),
            of(tab),
        )
    }),
    // Save profile
    mergeMap(([profileView, positionView, profileSkills, profileContacts, profileRecommendations, imageID, username, profile, tab]) =>
        // Define whether to create or update profile
        post(profile.hasOwnProperty('id') ? hostUrl + '/profile/' + profile.id : hostUrl + '/profile', JSON.stringify({
            firstName: profileView.profile.firstName,
            lastName: profileView.profile.lastName,
            label: profileView.profile.headline,
            email: profileContacts.emailAddress,
            picture: profile.hasOwnProperty('avatar') ? profile.avatar : (imageID ? imageID.id : null),
            // phone: profileContacts.emailAddress,
            // skype: profileContacts.emailAddress,
            summary: profileView.profile.summary,
            address: profileView.profile.locationName,
            skills: profileSkills.elements.map(element => {
                return {
                    name: element.skill.name,
                }
            }),
            profiles: (() => {
                let profiles = [];

                if (profileContacts.hasOwnProperty('ims')) {
                    profileContacts.ims.map(element => {
                        profiles = [...profiles, {
                            network: element.provider.toLowerCase(),
                            username: element.id,
                            url: '#'
                        }];
                    });
                }


                if (profileContacts.hasOwnProperty('twitterHandles')) {
                    profileContacts.twitterHandles.map(element => {
                        profiles = [...profiles, {
                            network: 'twitter',
                            username: element.name,
                            url: '#'
                        }];
                    });
                }

                if (profileContacts.hasOwnProperty('websites')) {
                    profileContacts.websites.map(element => {
                        let network = null;
                        Object.keys(element.type).map(key => {
                            Object.keys(element.type[key]).map(k => {
                                network = element.type[key][k].toLowerCase();
                            });
                        });
                        profiles = [...profiles, {
                            network: network,
                            username: null,
                            url: element.url
                        }];
                    });
                }
                // Linkedin
                profiles = [...profiles, {
                    network: 'linkedin',
                    username: username,
                    url: 'https://www.linkedin.com/in/' + username + '/'
                }];

                return profiles;
            })(),
            work: (() => {
                let positions = [];
                positionView.elements.map(element => {
                    element.positions.map(position => {
                        positions = [...positions, {
                            company: position.companyName,
                            position: position.title,
                            summary: position.description,
                            startDate: getDate('startDate', element),
                            endDate: getDate('endDate', element),
                        }]
                    })
                });
                return positions;
            })(),
            educations: profileView.educationView.elements.map(element => {
                return {
                    school: element.schoolName,
                    degree: element.degreeName,
                    fieldOfStudy: element.fieldOfStudy,
                    startDate: getDate('startDate', element),
                    endDate: getDate('endDate', element),
                }
            }),
            languages: profileView.languageView.elements.map(element => {
                return {
                    language: element.name,
                    fluency: getLanguageLevel(element.proficiency),
                };
            }),
            references: profileRecommendations.elements.map(element => {
                return {
                    name: element.recommender.firstName + element.recommender.lastName,
                    reference: element.recommendationText
                };
            }),
        }), {'Content-type': 'application/json; charset=utf-8'}).pipe(
            tap(result => {
                chrome.tabs.sendMessage(tab.tabId, {status: (profile.hasOwnProperty('id') ? 'exists' : 'success')});
            })
        )
    )
).subscribe();


function getDate(name, object) {
    let dateString = null;
    if (getObject(['timePeriod', name, 'year'], object)) {
        const endDate = new Date();
        endDate.setDate(1);
        endDate.setFullYear(getObject(['timePeriod', name, 'year'], object));
        if (getObject(['timePeriod', name, 'month'], object)) {
            endDate.setMonth(getObject(['timePeriod', name, 'month'], object) - 1);
        }
        dateString = moment(endDate).format('YYYY-MM-DD');
    }
    return dateString;
}

function getLanguageLevel(level) {
    switch (level) {
        case 'ELEMENTARY':
            return 'A1';
        case 'LIMITED_WORKING':
            return 'A2';
        case 'PROFESSIONAL_WORKING':
            return 'B2';
        case 'FULL_PROFESSIONAL':
            return 'C1';
        case 'NATIVE_OR_BILINGUAL':
            return 'C2';
        default:
            return 'A1';
    }
}

function get(url, headers?): Observable<any> {
    return Observable.create(function (observer) {
        const Http = new XMLHttpRequest();
        Http.open('GET', url);
        if (headers) {
            Object.keys(headers).forEach((key) => {
                Http.setRequestHeader(key, headers[key]);
            });
        }
        Http.send();
        Http.onreadystatechange = (e) => {
            if (Http.readyState === 4 && Http.status === 200) {
                observer.next(JSON.parse(Http.responseText));

            }
        }
    });
}

function post(url, data, headers?): Observable<any> {
    return Observable.create(function (observer) {
        const Http = new XMLHttpRequest();
        Http.open('POST', url, true);
        if (headers) {
            Object.keys(headers).forEach((key) => {
                Http.setRequestHeader(key, headers[key]);
            });
        }
        Http.send(data);
        Http.onreadystatechange = (e) => {
            if (Http.readyState === 4 && Http.status === 200) {
                observer.next(JSON.parse(Http.responseText));
            }
        }
    });
}

function getImage(url): Observable<Blob> {
    return Observable.create(function (observer) {
        const Http = new XMLHttpRequest();
        Http.open('GET', url, true);
        Http.responseType = 'blob';
        Http.onload = (e) => {
            observer.next(Http.response);
        };
        Http.send();
    });
}

function getCookies(): Observable<Cookie> {
    return Observable.create(function (observer) {
        chrome.cookies.get({'url': 'https://www.linkedin.com', 'name': 'JSESSIONID'}, function (cookie) {
            if (cookie) {
                observer.next(JSON.parse(cookie.value));
            }
        });
    });
}

function getMessage(): Observable<{ type: string, message: any }> {
    return Observable.create(function (observer) {
        chrome.runtime.onMessage.addListener((message: { type: string, message: any }, sender, sendResponse) => {
            observer.next(message)
        });
    });
}

function getTabChanged(): Observable<{ tabId: any, url: any }> {
    return Observable.create(function (observer) {
        chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
            const matches = decodeURI(tab.url).match(/^(https:\/\/www.linkedin.com\/in\/)([a-zA-Zа-яА-Яёіїґ0-9 \-]+)\/$/i);

            if (tab.status === "complete" && !!matches) {
                console.log(matches);
                observer.next({tabId: tabId, url: tab.url});
            }
        });
    })
}


const getObject = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
