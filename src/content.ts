import * as jQuery from 'jquery';

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    const container = jQuery('.t-24');
    const fetchStatus = container.find('.fetch-status');
    if (fetchStatus.length) {
        fetchStatus.remove();
    }
    switch (message.status) {
        case 'start':
            container.append('<span class="fetch-status" style="color: orange">(Saving...)</span>');
            break;
        case 'success':
            container.append('<span class="fetch-status" style="color: green">(Saved)</span>');
            break;
        case 'exists':
            container.append('<span class="fetch-status" style="color: blue">(Exists)</span>');
            break;
        default:
            container.append('<span class="fetch-status" style="color: red">(Failed)</span>');
    }
});
